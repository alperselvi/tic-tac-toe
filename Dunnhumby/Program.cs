﻿using System;
using Dunnhumby.Classes;

namespace Dunnhumby
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Tic Tac Toe");

            while (true)
            {
                Console.WriteLine("Creating a Tic Tac Toe board");

                TicTacToeBoard currentBoard = new TicTacToeBoard("X");
                Console.WriteLine("Created the board");
                Console.WriteLine("");

                Console.WriteLine("We will start with player X");
                Console.WriteLine("");

                currentBoard.PrintBoard();
                Console.WriteLine("");

                while (currentBoard.PossibleMovesExists())
                {
                    Console.WriteLine("Player " + currentBoard.GetCurrentPlayer() + " Please type next move in format of column and row (A1, B3)");
                    string nextMove = Console.ReadLine();

                    Result result = currentBoard.ProcessMove(nextMove);

                    Console.WriteLine(result.Message);
                    Console.WriteLine("");

                    currentBoard.PrintBoard();
                    Console.WriteLine("");
                }

                Console.WriteLine("Do you want to play again. Y/N");
                string answer = Console.ReadLine();

                if (answer.ToUpper() != "Y")
                    break;
            }
        }
    }
}
