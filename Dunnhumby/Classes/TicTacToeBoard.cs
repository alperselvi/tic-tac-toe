﻿using System;
using System.Linq;
using System.Text;
using Dunnhumby.Interfaces;

namespace Dunnhumby.Classes
{
    public class TicTacToeBoard : GameBoard
    {
        public TicTacToeBoard(string startingPlayer) : base(3, startingPlayer)
        {
        }
    }
}
