﻿using System;
using System.Linq;
using System.Text;
using Dunnhumby.Interfaces;

namespace Dunnhumby.Classes
{
    public class GameBoard : Board
    {
        private int[][] _board;
        private string[][] _boardString;

        private int _boardSize;
        private int _turnCount = 0;

        private bool _playerWon = false;
        private string _currentPlayer;

        private string alphabet = "";

        public GameBoard(int boardSize, string startingPlayer)
        {
            _boardSize = boardSize;
            _currentPlayer = startingPlayer;

            //Initial assumption that board size can not be greater than 26 (size of alphabet)
            if (_boardSize > 26)
                _boardSize = 26;

            for (char c = 'A'; c <= ('A' + _boardSize - 1); ++c)
            {
                alphabet += c.ToString();
            }

            _board = new int[_boardSize][];
            _boardString = new string[_boardSize][];

            for (int i = 0; i < _boardSize; i++)
            {
                _board[i] = Enumerable.Repeat(0, _boardSize).ToArray();
                _boardString[i] = Enumerable.Repeat("-", _boardSize).ToArray();
            }
        }

        public Result ProcessMove(string cell)
        {
            if (!PossibleMovesExists())
                return new Result() { Success = false, Message = "No possible moves left" };

            if (cell.Length < 2)
                return new Result() { Success = false, Message = "Cell value is incorrect" };

            string column = cell.Substring(0, 1);
            string rowValue = cell.Substring(1);
            int row = 0;

            if (!int.TryParse(rowValue, out row))
                return new Result() { Success = false, Message = "Cell value is incorrect" };

            //Index of row is 1 smaller than the screen
            row = row - 1;

            if (row >= _boardSize || row < 0 || column.Length != 1 || !alphabet.Contains(column.ToUpper()))
                return new Result() { Success = false, Message = "The cell is not in the bounds of board" };

            int columnIndex = alphabet.IndexOf(column.ToUpper());

            if (_board[row][columnIndex] != 0)
                return new Result() { Success = false, Message = "The cell is not available" };

            string lastPlayer = _currentPlayer;

            _turnCount++;

            _board[row][columnIndex] = (_currentPlayer == "X" ? 1 : 2);
            _boardString[row][columnIndex] = _currentPlayer;

            if (CheckWinConditions())
                return new Result() { Success = true, Message = "Game over, player " + lastPlayer + " wins" };
            else
            {
                if (IsDraw())
                    return new Result() { Success = true, Message = "Game over, it is a draw" };
                else
                {
                    _currentPlayer = (_currentPlayer == "X" ? "O" : "X");
                    return new Result() { Success = true, Message = "Game continues" };
                }
            }
        }

        public string GetCurrentPlayer()
        {
            return _currentPlayer;
        }

        public string GetWinnerPlayer()
        {
            if (PossibleMovesExists())
                return "Game is not over yet";

            return !IsDraw() ? _currentPlayer : "Draw";
        }

        public bool IsDraw()
        {
            if (!PossibleMovesExists() && !_playerWon)
                return true;

            return false;
        }

        private bool CheckWinConditions()
        {
            //Minimum number of turns required for a player to win
            if (_turnCount < (_boardSize * 2) - 1)
                return false;

            _playerWon = CheckHorizontalWinConditions() || CheckVerticalWinConditions() || CheckTopDiagonalWinConditions() || CheckBottomDiagonalWinConditions();

            return _playerWon;
        }

        private bool CheckHorizontalWinConditions()
        {
            for (int i = 0; i < _boardSize; i++)
            {
                int sum = 0;

                for (int j = 0; j < _boardSize; j++)
                {
                    int cellValue = _board[j][i];
                    if (cellValue == 0)
                    {
                        sum = 0;
                        break;
                    }

                    sum += cellValue;
                }

                if (sum > 0 && (sum == _boardSize || sum == _boardSize * 2))
                    return true;
            }

            return false;
        }

        private bool CheckVerticalWinConditions()
        {
            for (int i = 0; i < _boardSize; i++)
            {
                int sum = 0;

                for (int j = 0; j < _boardSize; j++)
                {
                    int cellValue = _board[i][j];
                    if (cellValue == 0)
                    {
                        sum = 0;
                        break;
                    }

                    sum += cellValue;
                }

                if (sum > 0 && (sum == _boardSize || sum == _boardSize * 2))
                    return true;
            }

            return false;
        }

        private bool CheckTopDiagonalWinConditions()
        {
            //If board size is divisible by 2, there are no diagonal options
            if (_boardSize % 2 == 0)
                return false;

            int sum = 0;

            for (int i = 0; i < _boardSize; i++)
            {
                int cellValue = _board[i][i];

                if (cellValue == 0)
                    return false;

                sum += cellValue;
            }

            if (sum % _boardSize == 0)
                return true;

            return false;
        }

        private bool CheckBottomDiagonalWinConditions()
        {
            //If board size is divisible by 2, there are no diagonal options
            if (_boardSize % 2 == 0)
                return false;

            int sum = 0;

            for (int i = 0; i < _boardSize; i++)
            {
                int cellValue = _board[_boardSize - 1 - i][i];

                if (cellValue == 0)
                    return false;

                sum += cellValue;
            }

            if (sum % _boardSize == 0)
                return true;

            return false;
        }

        public bool PossibleMovesExists()
        {
            if (_playerWon)
                return false;

            if (_turnCount == Math.Pow(_boardSize, 2))
                return false;

            return true;
        }

        public void PrintBoard()
        {
            Console.WriteLine("  " + string.Join("  ", alphabet.ToArray()));

            for (int i = 0; i < _boardSize; i++)
            {
                Console.WriteLine((i + 1) + " " + string.Join("  ", _boardString[i]));
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < _boardSize; i++)
            {
                sb.AppendLine(string.Join("  ", _boardString[i]));
            }

            return sb.ToString();
        }
    }
}
