﻿using System;
namespace Dunnhumby.Classes
{
    public class Result
    {
        public string Message;
        public bool Success;
    }
}
