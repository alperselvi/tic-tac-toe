﻿using System;
using Dunnhumby.Classes;

namespace Dunnhumby.Interfaces
{
    public interface Board
    {
        Result ProcessMove(string cell);
        string GetCurrentPlayer();
        string GetWinnerPlayer();
        bool IsDraw();
        bool PossibleMovesExists();
        void PrintBoard();
    }
}
