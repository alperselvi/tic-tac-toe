﻿using System;
using Dunnhumby.Classes;
using NUnit.Framework;

namespace Dunnhumby.Tests
{
    [TestFixture()]
    public class TicTacToeBoardTest
    {
        private TicTacToeBoard board;

        [SetUp]
        public void CreateBoard()
        {
            board = new TicTacToeBoard("X");
        }

        [Test()]
        public void NewGame_PlayerXGoesFirst_ReturnsSuccess()
        {
            Assert.That(board.GetCurrentPlayer(), Is.EqualTo("X"));
        }

        [Test]
        public void SecondMove_PlayerO_ReturnsSuccess()
        {
            board.ProcessMove("A1");
            Assert.That(board.GetCurrentPlayer(), Is.EqualTo("O"));
        }

        [Test]
        public void NewGame_PossibleMovesExists_ReturnsTrue()
        {
            Assert.That(board.PossibleMovesExists, Is.True);
        }

        [Test]
        public void ThreeInARow_PossibleMovesExists_ReturnsFalse()
        {
            board.ProcessMove("A1");
            board.ProcessMove("C1");
            board.ProcessMove("A2");
            board.ProcessMove("C2");
            board.ProcessMove("A3");
            Assert.That(board.PossibleMovesExists, Is.False);
        }

        [Test]
        public void PlayerX_ThreeInFirstRow_Winner_ReturnsPlayerX()
        {
            board.ProcessMove("A1");
            board.ProcessMove("C1");
            board.ProcessMove("A2");
            board.ProcessMove("C2");
            board.ProcessMove("A3");
            Assert.That(board.GetWinnerPlayer(), Is.EqualTo("X"));
        }

        [Test]
        public void PlayerO_ThreeInSecondRow_Winner_ReturnsPlayerO()
        {
            board.ProcessMove("A1");
            board.ProcessMove("B1");
            board.ProcessMove("A2");
            board.ProcessMove("B2");
            board.ProcessMove("C3");
            board.ProcessMove("B3");
            Assert.That(board.GetWinnerPlayer(), Is.EqualTo("O"));
        }

        [Test]
        public void PlayerX_ThreeInThirdRow_Winner_ReturnsPlayerX()
        {
            board.ProcessMove("C1");
            board.ProcessMove("A1");
            board.ProcessMove("C2");
            board.ProcessMove("A2");
            board.ProcessMove("C3");
            Assert.That(board.GetWinnerPlayer(), Is.EqualTo("X"));
        }

        [Test]
        public void PlayerO_ThreeInFirstColumn_Winner_ReturnsPlayerO()
        {
            board.ProcessMove("A3");
            board.ProcessMove("A1");
            board.ProcessMove("A2");
            board.ProcessMove("B1");
            board.ProcessMove("C3");
            board.ProcessMove("C1");
            Assert.That(board.GetWinnerPlayer(), Is.EqualTo("O"));
        }

        [Test]
        public void PlayerX_ThreeInSecondColumn_Winner_ReturnsPlayerX()
        {
            board.ProcessMove("A2");
            board.ProcessMove("A1");
            board.ProcessMove("B2");
            board.ProcessMove("B1");
            board.ProcessMove("C2");
            Assert.That(board.GetWinnerPlayer(), Is.EqualTo("X"));
        }

        [Test]
        public void PlayerO_ThreeInThirdColumn_Winner_ReturnsPlayerO()
        {
            board.ProcessMove("A1");
            board.ProcessMove("A3");
            board.ProcessMove("A2");
            board.ProcessMove("B3");
            board.ProcessMove("C2");
            board.ProcessMove("C3");
            Assert.That(board.GetWinnerPlayer(), Is.EqualTo("O"));
        }

        [Test]
        public void PlayerX_ThreeInTopDiagonal_Winner_ReturnsPlayerX()
        {
            board.ProcessMove("A1");
            board.ProcessMove("A2");
            board.ProcessMove("B2");
            board.ProcessMove("B1");
            board.ProcessMove("C3");
            Assert.That(board.GetWinnerPlayer(), Is.EqualTo("X"));
        }

        [Test]
        public void PlayerO_ThreeInBottomDiagonal_Winner_ReturnsPlayerO()
        {
            board.ProcessMove("A1");
            board.ProcessMove("A3");
            board.ProcessMove("A2");
            board.ProcessMove("B2");
            board.ProcessMove("C3");
            board.ProcessMove("C1");
            Assert.That(board.GetWinnerPlayer(), Is.EqualTo("O"));
        }

        [Test]
        public void ValidMove_ReturnsSuccessful()
        {
            Result result = board.ProcessMove("A1");

            Assert.That(result.Success, Is.True);
        }

        [Test]
        public void InvalidMove_AlreadyPlayed_ReturnsError()
        {
            board.ProcessMove("A1");
            Result result = board.ProcessMove("A1");

            Assert.That(result.Success, Is.False);
        }

        [Test]
        public void InvalidMove_NoRowNumber_ReturnsError()
        {
            Result result = board.ProcessMove("AA");

            Assert.That(result.Success, Is.False);
        }

        [Test]
        public void InvalidMove_NoColumn_ReturnsError()
        {
            Result result = board.ProcessMove("11");

            Assert.That(result.Success, Is.False);
        }

        [Test]
        public void InvalidMove_OutOfBounds_ReturnsError()
        {
            Result result = board.ProcessMove("D6");

            Assert.That(result.Success, Is.False);
        }

        [Test]
        public void NoPossibleMoves_NoWinner_Draw_ReturnsTrue()
        {
            board.ProcessMove("A1");
            board.ProcessMove("A2");
            board.ProcessMove("A3");
            board.ProcessMove("B1");
            board.ProcessMove("B2");
            board.ProcessMove("C1");
            board.ProcessMove("B3");
            board.ProcessMove("C3");
            board.ProcessMove("C2");
            Assert.That(board.IsDraw(), Is.True);
        }

        [Test]
        public void Draw_PossibleMovesExists_ReturnsFalse()
        {
            board.ProcessMove("A1");
            board.ProcessMove("A2");
            board.ProcessMove("A3");
            board.ProcessMove("B1");
            board.ProcessMove("B2");
            board.ProcessMove("C1");
            board.ProcessMove("B3");
            board.ProcessMove("C3");
            board.ProcessMove("C2");
            Assert.That(board.PossibleMovesExists(), Is.False);
        }

        [Test]
        public void Board_ToString_FourLines_ReturnsSuccess()
        {
            Assert.That(board.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None).Length, Is.EqualTo(4));
        }
    }
}
